var waveClient = new API();

var reports = {
	dt : null,
	load : function(){
		var attachTable = function(){
			if (reports.dt != null) return;
			
			var responsiveHelper_reportTable = undefined;
			
			var breakpointDefinition = {
				tablet : 1024,
				phone : 480
			};
			
			reports.dt = $('#reportTable').DataTable({
				"order": [[ 0, 'asc' ], [ 2, 'asc' ]],
				columns: [
					{
		              "title": "Employee ID",
		              "data": "employeeID",
		              "orderable": false
			        },
			        {
		              "title": "Pay Period",
		              "data": "period",
		              //visible : false,
		              "orderable": false,
		              type: "datetime-moment", 
		              targets: "sort-date"
		            },
			        {
		              "title": "Period Start",
		              "data": "start",
		              visible : false,
		              "orderable": false,
		              type: "datetime-moment", 
		              targets: "sort-date",
		              render: function (data, type, row) {
		                  return moment(data).format("MM/DD/YYYY")
		              }
		            },
		            {
		         
		              "title": "Period End",
		              "data": "end",
		              visible : false,
		              "orderable": false,
		              type: "datetime-moment", 
		              targets: "sort-date",
		              render: function (data, type, row) {
		                  return moment(data).format("MM/DD/YYYY")
		              }
		            },
		            
		            {
		              "title": "Amount Paid",
		              "data": "paid",
		              "orderable": false,
		              render: function (data, type, row) {
		                  return "$"+data
		              }
		            }
		         ],
				"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
					"t"+
					"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
				"autoWidth" : true,
		        "oLanguage": {
				    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
				},
				"preDrawCallback" : function() {
					// Initialize the responsive datatables helper once.
					if (!responsiveHelper_reportTable) {
						responsiveHelper_reportTable = new ResponsiveDatatablesHelper($('#reportTable'), breakpointDefinition);
					}
				},
				"rowCallback" : function(nRow) {
					responsiveHelper_reportTable.createExpandIcon(nRow);
				},
				"drawCallback" : function(oSettings) {
					responsiveHelper_reportTable.respond();
				}
			});
		}
		
		if (reports.dt == null){
			attachTable();
		}
		
		var loadData = function(entries){
			reports.dt.clear().draw();
			
			$.each(entries, function(index, entry){
				reports.dt.row.add(entry).draw();
			});			
		}
		
		waveClient.entries.getPeriods(function(entries){
			loadData(entries);		
		})
	}
};


(function (factory) {
    if (typeof define === "function" && define.amd) {
        define(["jquery", "moment", "datatables.net"], factory);
    } else {
        factory(jQuery, moment);
    }
}(function ($, moment) { 
	$.fn.dataTable.moment = function ( format, locale ) {
	    var types = $.fn.dataTable.ext.type;
	 
	    // Add type detection
	    types.detect.unshift( function ( d ) {
	        if ( d ) {
	            // Strip HTML tags and newline characters if possible
	            if ( d.replace ) {
	                d = d.replace(/(<.*?>)|(\r?\n|\r)/g, '');
	            }
	 
	            // Strip out surrounding white space
	            d = $.trim( d );
	        }
	 
	        // Null and empty values are acceptable
	        if ( d === '' || d === null ) {
	            return 'moment-'+format;
	        }
	 
	        return moment( d, format, locale, true ).isValid() ?
	            'moment-'+format :
	            null;
	    } );
	 
	    // Add sorting method - use an integer for the sorting
	    types.order[ 'moment-'+format+'-pre' ] = function ( d ) {
	        if ( d ) {
	            // Strip HTML tags and newline characters if possible
	            if ( d.replace ) {
	                d = d.replace(/(<.*?>)|(\r?\n|\r)/g, '');
	            }
	 
	            // Strip out surrounding white space
	            d = $.trim( d );
	        }
	         
	        return d === '' || d === null ?
	            -Infinity :
	            parseInt( moment( d, format, locale, true ).format( 'x' ), 10 );
	    };
	};
	 
}));