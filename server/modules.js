const fs = require('fs')
const path = require('path');
const async = require('async');
global.appRoot = path.resolve(__dirname);

const express = require('express')
	, router = express.Router();

var EventEmitter = require('events').EventEmitter;
var util = require('util');
var Sequelize = require('sequelize');

var env       = process.env.NODE_ENV || 'development';
var config    = require(__dirname + '/config/config.json')[env];

if (config.use_env_variable) {
  var sequelize = new Sequelize(process.env[config.use_env_variable]);
} else {
  var sequelize = new Sequelize(config.database, config.username, config.password, config);
}

function Modules(app, done) {
	var self = this;
	
    var loadedModules = {};
    
    EventEmitter.call(this);
    util.inherits(app, EventEmitter);
    
    this.tables = {};
     		
	this.getModule = function(module){
		return loadedModules[module]
	}
	
	this.loadRoutes = function(){
		return loadRoutes();
	}
	
	init(app, function(loaded, loadedModels){
		loadedModules = loaded;

		tables = loadedModels;
		
		Object.keys(tables).forEach(function(modelName) {
		  if (tables[modelName].associate) {
			  tables[modelName].associate(tables);
		  }
		});

		//This forces the creation / update of the SQL tables
		sequelize.sync({  
			//force: true //On during development
		}).then(function() {

		}).catch(function(err) {
		    // print the error details
		    console.error(err);
		});
		
		done(self);
	});
}

module.exports = Modules;

function init(app, done){
	var loadedModules = {};
	var moduleFolder = appRoot + '/modules/';
	var loadedDBs = {};

	var dirs = fs.readdirSync(moduleFolder).filter(
			file => fs.lstatSync(
				path.join(moduleFolder, file)
			).isDirectory()
		);
			
	async.eachSeries(dirs, function(module, nextModule){
		if (fs.existsSync(moduleFolder + module + "/index.js")) {
			var thisModule = require(moduleFolder + module + "/index.js");
			
			if (typeof thisModule == "function") {
				thisModule = new thisModule(app);
			} else {
				if (thisModule.preLoad){
					if (typeof thisModule.preLoad == 'function'){
						app.on('modules.preLoad', thisModule.preLoad);
					}					
				}
				
				if (thisModule.load){
					if (typeof thisModule.load == 'function'){
						app.on('modules.load', thisModule.load);
					}
				}
				
				
				if (thisModule.postLoad){
					if (typeof thisModule.postLoad == 'function'){
						app.on('modules.postLoad', thisModule.postLoad);
					}
				}
			}
			
			if (thisModule.model){
				var model = sequelize['import'](thisModule.model);
				loadedDBs[model.name] = model;
			}
			
			if (thisModule.routes && typeof thisModule.routes == 'function'){
				app.use('/', loadRoutes(module, thisModule.routes(thisModule.app)));
			}
			
			loadedModules[module] = thisModule.app;
		}
		
		nextModule();
	}, function(){
		app.emit('modules.preLoad');
		app.emit('modules.load');		
		app.emit('modules.postLoad');

		done(loadedModules, loadedDBs);
	});
}

function loadRoutes(module, modRoutes){			
	modRoutes.forEach(function(route) {
		if (route.type == "param"){
			routes.param(module, router, route, {});
		} else if (route.type == "route"){
			routes.route(module, router, route, null, {});
		} 			
	});
	
	return router;
}


var routes = {
	param : function(module, router, route, options){

		router.param(route.param, route.callback);
	},
	
	route : function(module, router, route, passport, options){
		var callback = [];
		
		if (route.callback instanceof Function){
			route.callback = [route.callback];
		} else if (route.callback instanceof Array){
			
		} else {
			console.error("Bad route callback.");
		}
		
		//Access restriction
		if (route.access == "public"){
			
		} else if (route.access == "private"){
			console.error("To implement...");
		} else if (route.access == "oauth"){
			console.error("To implement...");
		} else {
			console.error("Bad route auth type");
		}
		callback = callback.concat(route.callback);
		
		var routePath = "/api/" + module + "/" + route.route;
		if (route.verb == "get"){
			router.route(routePath).get(callback);
		} else if (route.verb == "post"){
			router.route(routePath).post(callback);
		} else if (route.verb == "put"){
			router.route(routePath).put(callback);
		} else if (route.verb == "delete"){
			router.route(routePath).delete(callback);
		} else {
			console.error("Bad route verb");
		}
	}
};