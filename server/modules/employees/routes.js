module.exports = function(thisApp){

	return [		
		{
			type : "route",
			route : "",
			verb : "get",
			access : "public",
			callback : function(req, res, next) {
				thisApp.getAll({req : req}, function(data){
					res.json(data);	
				});
			}
		},
		
		{
			type : "route",
			route : "",
			verb : "post",
			access : "public",
			callback : function(req, res, next) {
				thisApp.add(req.body, {req : req}, function(data){
					res.json(data);
				});
			}
		}
	];
};
