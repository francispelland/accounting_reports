const fs = require('fs');
const path = require('path');
var appRoot = path.resolve(__dirname);

function Employees(app) {	
	
	app.on('modules.preLoad', function(){
		
	});
	
	app.on('modules.postLoad', function(){	
			
	});
	
	this.app = require(appRoot + '/app.js');
	this.model = appRoot + '/model.js';
	this.routes = require(appRoot + '/routes.js');	
}

module.exports = Employees;