'use strict';
module.exports = function(sequelize, DataTypes) {
  var Employee = sequelize.define('Employee', {
	employeeID: {
        type: DataTypes.INTEGER,
        primaryKey: true
	},
	name: {
		type: DataTypes.STRING
	},
	jobGroup : {
		type: DataTypes.STRING
	}
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return Employee;
};