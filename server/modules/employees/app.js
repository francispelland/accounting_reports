

module.exports = {
	getAll : function(options, done){
		const employeeTable = global.modules.tables.Employee;

		employeeTable.findAll({}).then(function(employees) {
			done(employees);
		});
	},
		
	get : function(employeeID, options, done){
		const employeeTable = global.modules.tables.Employee;

		employeeTable.find({
			where: {
		      employeeID: employeeID
		    }
		}).then(function(employee) {
			  done(employee);
		});
	},
	
	set : function(data, options, done){
		const employeeTable = global.modules.tables.Employee;

		employeeTable.upsert(data).then(function(entry) {
			done(entry);
		});
		
	},
	
	update : function(employeeID, data, options, done){
		const employeeTable = global.modules.tables.Employee;
		
		employeeTable.find({
			where: {
				employeeID: employeeID
			}
		}).then(function(employee) {
			if(employee){
				todo.updateAttributes(data).then(function(employee) {
					done(employee);
				});
			}
		});
	}
}