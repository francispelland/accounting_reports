const uuidv4 = require('uuid/v4')
	, async = require('async')
	, moment = require('moment');

module.exports = {
	getAll : function(options, done){
		const entryTable = global.modules.tables.Entry;
		
		entryTable.findAll({
			include : [global.modules.tables.Employee],
			raw : true
		}).then(function(entries) {
			done(entries);
		});
	},
	
	getByReport : function(reportID, options, done){
		const entryTable = global.modules.tables.Entry;
		
		entryTable.findAll({
			where: {
				reportID: reportID
			},
			raw : true
		}).then(function(entries) {
		
			done(entries);
		});
	},
	
	getPeriods : function(options, done){
		const entryTable = global.modules.tables.Entry;
		var finalData = {};		
		
		module.exports.getAll(options, function(entries){
			entries.forEach(function(entry){
				var firstPeriodDay = moment(entry.date).date() <= 15 ? 1 : 16;

				var startPeriod = moment(entry.date).set('date', firstPeriodDay);
				if (firstPeriodDay == 1){
					var endPeriod = moment(entry.date).set('date', 15);
				} else {
					var endPeriod = moment(entry.date).set(1, 'date').add(1, 'month').subtract(1, 'day');
				}

				var hourlyAmount = entry['Employee.jobGroup'] == "A" ? 20 : 30;
				var key = entry.employeeID + "_" + startPeriod.unix();
				if (!finalData[key]){
					finalData[key] = {
						start : startPeriod.toDate(),
						end : endPeriod.toDate(),
						period : startPeriod.format("MM/DD/YYYY") + " - " + endPeriod.format("MM/DD/YYYY"),
						employeeID : entry.employeeID,
						paid : (entry.hours * hourlyAmount)
					}
				} else {
					finalData[key].paid += (entry.hours * hourlyAmount);
				}
			});
			
			finalData = Object.values(finalData);

			done(finalData);
		});
	},
	
	add : function(data, options, done){
		const entryTable = global.modules.tables.Entry;
		
		if (typeof data.date === "string"){
			data.date = moment(data.date, "DD/MM/YYYY").toDate();
		}

		entryTable.create(data).then(function(entry) {
			done(entry);
		});
		
	},
	
	update : function(entryID, data, options, done){
		
	},
	
	remove : function(entryID, options, done){
		models.Entry.destroy({
			where: {
				id: req.params.id
			}
		}).then(function(entry) {
			res.json(entry);
		});
	},
	
	importCSV : function(files, options, done){
		Object.keys(files).forEach(function (key) {
			let file = files[key];
			
			var filePath = global.appRoot+'/uploads/' + uuidv4() + ".csv";
			file.mv(filePath, function(err) {
				parseCSV(filePath, function(err, result){
					if (err){
						return options.res.status(500).send(err.message);
					}						
					
					done({status : 1})
				})				
			});
		});
	}
}


function parseCSV(filePath, done){
	var fs = require('fs');
	var csv = require('csv');
	
	var validHeader = function(data){
		var header = data[0];
		
		if (header[0] != "date" ||
			header[1] != "hours worked" ||
			header[2] != "employee id" ||
			header[3] != "job group")
			return false;
		else
			return true;
	}
	
	var validFooter = function(data){
		var footer = data[data.length - 1];

		if (footer[0] != "report id" ||
			parseInt(footer[1]) <= 0)
				return false;
			else {
				return true;
			}	
	}
	
	var checkReport = function(data, checkCB){
		var footer = data[data.length - 1];

		var reportID = parseInt(footer[1]);
		
		module.exports.getByReport(reportID, {}, function(data){
			if (data.length > 0){
				checkCB(true, reportID); //It exists
			} else {
				checkCB(false, reportID);
			}
		})
		return reportID;
	}
	

	var parser = csv.parse({delimiter: ','}, function(err, data){
		const employees = global.modules.getModule('employees');
		
		var reportID = 0;
		if (!validHeader(data) || !validFooter(data)){
			return done(new Error("Invalid header or footer."));
		}
		
		checkReport(data, function(err, reportID){
			if (err){
				return done(new Error("Report with ID " + reportID + " has already been uploaded."));
			}
			
			var rows = data;
			rows.shift();  // Removes the first row
			rows.pop();    // Removes the last row
			
			async.each(rows, function(row, rowCB) {
				var entryData = {
					reportID : reportID,
					date : row[0],
					hours : row[1],
					employeeID : row[2]
				}
				
				var employeeData = {
					employeeID : row[2],
					name : "Employee " + row[2],
					jobGroup : row[3]
				}
				
				employees.set(employeeData, {}, function(employee){
					module.exports.add(entryData, {}, function(retVal){
						rowCB()
					});
				});				
			}, function(err) {
				done(null, {})
			});			
		})
		
	});

	fs.createReadStream(filePath).pipe(parser);
}