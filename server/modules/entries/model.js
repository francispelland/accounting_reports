'use strict';
module.exports = function(sequelize, DataTypes) {
  var Entry = sequelize.define('Entry', {
	  entryID : {
		  type : DataTypes.INTEGER,
		  primaryKey : true,
		  autoIncrement : true
	  },
	  reportID: {
		  type: DataTypes.INTEGER
	  },
	  employeeID: {
		  type: DataTypes.INTEGER
	  },
	  date: {
		  type: DataTypes.DATE
	  },
	  hours : {
		  type: DataTypes.DECIMAL(10,1)
	  }
  }, {
    classMethods: {
      associate: function(models) {
    	  Entry.belongsTo(models.Employee, {foreignKey: 'employeeID', targetKey: 'employeeID'});
      }
    }
  });
  return Entry;
};