const fs = require('fs');
const path = require('path');
var appRoot = path.resolve(__dirname);

function Entries(app) {	
	app.on('modules.preLoad', function(){
		if (!fs.existsSync(global.appRoot + "/uploads")){
		    fs.mkdirSync(global.appRoot + "/uploads");
		}
	});
	
	app.on('modules.postLoad', function(){	
			
	});
	
	this.app = require(appRoot + '/app.js');
	this.model = appRoot + '/model.js';
	this.routes = require(appRoot + '/routes.js');
	
}

module.exports = Entries;